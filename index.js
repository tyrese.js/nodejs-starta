
var express = require('express');
var app = express();
var path = require('path');
var os = require('os');
var bodyParser = require('body-parser');
var fs = require('fs');

app.use(function log (req, res, next) {
  console.log([req.method, req.url].join(' '));
  next();
});
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }))

app.set('view engine', 'jade');

app.get('/', function(req, res) {
    res.send("Hello, welcome to the Cinemaloop-Fandango something.");
});

var server = app.listen(process.env.PORT || 3000, function() {
    console.log('Listening on %s', process.env.PORT);
});
